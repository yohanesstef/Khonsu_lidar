/*
 * yaw_motion.h
 *
 *  Created on: Nov 6, 2022
 *      Author: ahmadjabar
 */

#ifndef INC_APPLICATION_MOTION_H_
#define INC_APPLICATION_MOTION_H_

#include <main.h>
#include <tim.h>
#include <driver/motor.h>
#include <driver/pwm.h>
#include <driver/encoder.h>
#include <pid.h>

void MOTION_YAW_Init();

void MOTION_PITCH_Init();

void MOTION_LINEAR_Init();

void MOTION_YAW_Callibration();

void MOTION_LINEAR_Callibration();

void MOTION_PITCH_Callibration();

void MOTION_LINEAR_Set_Position(float target_position);

void MOTION_YAW_Set_Position(float target_position);

void MOTION_PITCH_Set_Position(float target_position);

int16_t MOTION_LINEAR_Spin();

int16_t MOTION_YAW_Spin();

int16_t MOTION_PITCH_Spin();


#endif /* INC_APPLICATION_MOTION_H_ */
