/*
 * roller.h
 *
 *  Created on: Nov 6, 2022
 *      Author: ahmadjabar
 */

#ifndef INC_APPLICATION_ROLLER_H_
#define INC_APPLICATION_ROLLER_H_

#include <main.h>
#include <tim.h>
#include <driver/motor.h>
#include <driver/pwm.h>
#include <driver/encoder.h>
#include <pid.h>

void ROLLER_Init();

void ROLLER_Set_Speed(float target_speed);

void ROLLER_Spin(float * output);

#endif /* INC_APPLICATION_ROLLER_H_ */
