/*
 * encoder.h
 *
 *  Created on: Nov 5, 2022
 *      Author: ahmadjabar
 */

#ifndef INC_DRIVER_ENCODER_H_
#define INC_DRIVER_ENCODER_H_

#include <main.h>
#include <math.h>

typedef struct ENC_config
{
	TIM_TypeDef *T_pwm;
	TIM_HandleTypeDef *H_pwm;
	uint8_t channel;

	GPIO_TypeDef *port_1;
	GPIO_TypeDef *port_2;
	uint16_t pin_1;
	uint16_t pin_2;

} ENC_Block;

void ENC_Init(ENC_Block *encoder);
int16_t ENC_Get_Value(ENC_Block* encoder);
void ENC_Set_Value(ENC_Block* encoder, int16_t value);

#endif /* INC_DRIVER_ENCODER_H_ */
