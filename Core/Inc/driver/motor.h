/*
 * motor.h
 *
 *  Created on: Nov 5, 2022
 *      Author: ahmadjabar
 */

#ifndef INC_DRIVER_MOTOR_H_
#define INC_DRIVER_MOTOR_H_

#include <driver/pwm.h>
#include <pid.h>
#include <driver/encoder.h>

/**
 * @struct motor_driver_speed
 * @brief
 *
 */

typedef struct motor_driver_speed
{
	PWM_Block * pwm;
	pid_ctrl * pid;
	ENC_Block * encoder;
	float target_speed;

} MOTOR_Speed_Block;

/**
 * @struct motor_driver_posisi
 * @brief
 *
 */
typedef struct motor_driver_posisi
{
	PWM_Block * pwm;
	pid_ctrl * pid;
	ENC_Block * encoder;
	float target_position;

} MOTOR_Position_Block;

/**
 * @fn void MOTOR_Position_Init(MOTOR_Position_Block*)
 * @brief
 *
 * @param motorpos
 */

void MOTOR_Position_Init(MOTOR_Position_Block * motorpos);

/**
 * @fn void MOTOR_Speed_Init(MOTOR_Speed_Block*)
 * @brief
 *
 * @param motorspd
 */
void MOTOR_Speed_Init(MOTOR_Speed_Block *motorspd);

/**
 * @fn void MOTOR_Set_Speed(MOTOR_Speed_Block*, float)
 * @brief
 *
 * @param speed_block
 * @param target_speed
 */
void MOTOR_Set_Speed(MOTOR_Speed_Block * speed_block, float target_speed);

/**
 * @fn void MOTOR_Set_Position(MOTOR_Position_Block*, float)
 * @brief
 *
 * @param position_block
 * @param target_position
 */
void MOTOR_Set_Position(MOTOR_Position_Block * position_block, float target_position);

/**
 * @fn int16_t MOTOR_Spin_Position(MOTOR_Position_Block*)
 * @brief
 *
 * @pre
 * @post
 * @param position_block
 * @return current_pos
 */
int16_t MOTOR_Spin_Position(MOTOR_Position_Block * position_block);

/**
 * @fn int16_t MOTOR_Spin_Speed(MOTOR_Speed_Block*)
 * @brief
 *
 * @pre
 * @post
 * @param speed_block
 * @return current_speed
 */
int16_t MOTOR_Spin_Speed(MOTOR_Speed_Block * speed_block);



#endif /* INC_DRIVER_MOTOR_H_ */
