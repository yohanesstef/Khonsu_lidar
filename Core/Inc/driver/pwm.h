/*
 * pwm.h
 *
 *  Created on: Nov 5, 2022
 *      Author: ahmadjabar
 */

#ifndef INC_DRIVER_PWM_H_
#define INC_DRIVER_PWM_H_

#include <main.h>
#include <math.h>

typedef struct PWM_config
{
	TIM_TypeDef *T_pwm;
	TIM_HandleTypeDef *H_pwm;
	uint8_t channel;

	GPIO_TypeDef *port_1;
	GPIO_TypeDef *port_2;
	uint16_t pin_1;
	uint16_t pin_2;

} PWM_Block;

void PWM_Init(PWM_Block *pwm);
void PWM_Set_Value(PWM_Block* pwm, int16_t value);



#endif /* INC_DRIVER_PWM_H_ */
