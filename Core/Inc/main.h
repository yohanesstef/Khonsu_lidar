/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define M1_PWM_Pin GPIO_PIN_5
#define M1_PWM_GPIO_Port GPIOE
#define M2_PWM_Pin GPIO_PIN_6
#define M2_PWM_GPIO_Port GPIOE
#define M0_DIR_A_Pin GPIO_PIN_5
#define M0_DIR_A_GPIO_Port GPIOA
#define YAW_PWM_Pin GPIO_PIN_6
#define YAW_PWM_GPIO_Port GPIOA
#define M0_PWM_Pin GPIO_PIN_7
#define M0_PWM_GPIO_Port GPIOA
#define YAW_DIR_B_Pin GPIO_PIN_0
#define YAW_DIR_B_GPIO_Port GPIOB
#define YAW_DIR_A_Pin GPIO_PIN_1
#define YAW_DIR_A_GPIO_Port GPIOB
#define M2_DIR_B_Pin GPIO_PIN_2
#define M2_DIR_B_GPIO_Port GPIOB
#define M2_DIR_A_Pin GPIO_PIN_7
#define M2_DIR_A_GPIO_Port GPIOE
#define M0_ENC_A_Pin GPIO_PIN_9
#define M0_ENC_A_GPIO_Port GPIOE
#define M1_DIR_A_Pin GPIO_PIN_10
#define M1_DIR_A_GPIO_Port GPIOE
#define M0_ENC_B_Pin GPIO_PIN_11
#define M0_ENC_B_GPIO_Port GPIOE
#define M1_DIR_B_Pin GPIO_PIN_13
#define M1_DIR_B_GPIO_Port GPIOE
#define PITCH_DIR_B_Pin GPIO_PIN_12
#define PITCH_DIR_B_GPIO_Port GPIOB
#define PITCH_DIR_A_Pin GPIO_PIN_13
#define PITCH_DIR_A_GPIO_Port GPIOB
#define PITCH_PWM_Pin GPIO_PIN_15
#define PITCH_PWM_GPIO_Port GPIOB
#define M0_DIR_B_Pin GPIO_PIN_8
#define M0_DIR_B_GPIO_Port GPIOD
#define YAW_PROX_Pin GPIO_PIN_10
#define YAW_PROX_GPIO_Port GPIOD
#define PITCH_PROX_Pin GPIO_PIN_11
#define PITCH_PROX_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
