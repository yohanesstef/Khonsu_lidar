/*
 * pid.h
 *
 *  Created on: Nov 5, 2022
 *      Author: ahmadjabar
 */

#ifndef INC_PID_H_
#define INC_PID_H_

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/** @struct pid_ctrl
    @brief PID control block
*/
typedef struct
{
    float Kp; // PID Kp value
    float Ki; // PID Ki value
    float Kd; // PID Kd value
    float previous_err; // Previous error
    float integral_err;  // Sum of error
    //float last_output;  // PID output in last control period
    float max_output;   // PID maximum output limitation
    float min_output;   // PID minimum output limitation
    float max_integral; // PID maximum integral value limitation
    float min_integral; // PID minimum integral value limitation
    uint16_t dt; // PID time sampling (microseconds)
} pid_ctrl;

/**
 * @brief Input error and get PID control result
 * @param[in] pid PID control block
 * @param[in] input_error error data that feed to the PID controller
 * @param[out] output result after PID calculation
 * @return  STM_OK: Run a PID compute successfully\n
            STM_ERR_INVALID_ARG: Run a PID compute failed because of invalid argument
 */

float pid_compute(pid_ctrl *pid, float input_error);


#ifdef __cplusplus
}
#endif

#endif /* INC_PID_H_ */
