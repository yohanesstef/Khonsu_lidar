/*
 * yaw_motion.c
 *
 *  Created on: Nov 6, 2022
 *      Author: ahmadjabar
 */

#include <application/motion.h>


PWM_Block yaw_pwm = {
		.T_pwm = TIM12,
		.H_pwm = &htim9,
		.channel = TIM_CHANNEL_1,
		.port_1 = M0_DIR_A_GPIO_Port,
		.port_2 = M0_DIR_B_GPIO_Port,
		.pin_1 = M0_DIR_A_Pin,
		.pin_2 = M0_DIR_B_Pin
};

ENC_Block yaw_enc = {
		.T_pwm = TIM10,
		.H_pwm = &htim11,
		.channel = TIM_CHANNEL_1,
		.port_1 = M0_DIR_A_GPIO_Port,
		.port_2 = M0_DIR_B_GPIO_Port,
		.pin_1 = M0_DIR_A_Pin,
		.pin_2 = M0_DIR_B_Pin
};

pid_ctrl yaw_pid = {
		.Kd = 0,
		.Ki = 0,
		.Kp = 0,
		.previous_err = 0,
		.integral_err = 0,
		.max_output = 0,
		.min_output = 0,
		.max_integral = 0,
		.min_integral = 0,
		.dt = 1
};

MOTOR_Position_Block yaw_motor = {
		.pwm = &yaw_pwm,
		.encoder = &yaw_enc,
		.pid = &yaw_pid,
		.target_position = 0
};

/**

 */

PWM_Block pitch_pwm = {
		.T_pwm = TIM12,
		.H_pwm = &htim9,
		.channel = TIM_CHANNEL_1,
		.port_1 = M0_DIR_A_GPIO_Port,
		.port_2 = M0_DIR_B_GPIO_Port,
		.pin_1 = M0_DIR_A_Pin,
		.pin_2 = M0_DIR_B_Pin
};

ENC_Block pitch_enc = {
		.T_pwm = TIM10,
		.H_pwm = &htim11,
		.channel = TIM_CHANNEL_1,
		.port_1 = M0_DIR_A_GPIO_Port,
		.port_2 = M0_DIR_B_GPIO_Port,
		.pin_1 = M0_DIR_A_Pin,
		.pin_2 = M0_DIR_B_Pin
};

pid_ctrl pitch_pid = {
		.Kd = 0,
		.Ki = 0,
		.Kp = 0,
		.previous_err = 0,
		.integral_err = 0,
		.max_output = 0,
		.min_output = 0,
		.max_integral = 0,
		.min_integral = 0,
		.dt = 1
};

MOTOR_Position_Block pitch_motor = {
		.pwm = &pitch_pwm,
		.encoder = &pitch_enc,
		.pid = &pitch_pid,
		.target_position = 0
};

/*
 * */
PWM_Block linear_pwm = {
		.T_pwm = TIM12,
		.H_pwm = &htim9,
		.channel = TIM_CHANNEL_1,
		.port_1 = M0_DIR_A_GPIO_Port,
		.port_2 = M0_DIR_B_GPIO_Port,
		.pin_1 = M0_DIR_A_Pin,
		.pin_2 = M0_DIR_B_Pin
};

ENC_Block linear_enc = {
		.T_pwm = TIM10,
		.H_pwm = &htim11,
		.channel = TIM_CHANNEL_1,
		.port_1 = M0_DIR_A_GPIO_Port,
		.port_2 = M0_DIR_B_GPIO_Port,
		.pin_1 = M0_DIR_A_Pin,
		.pin_2 = M0_DIR_B_Pin
};

pid_ctrl linear_pid = {
		.Kd = 0,
		.Ki = 0,
		.Kp = 0,
		.previous_err = 0,
		.integral_err = 0,
		.max_output = 0,
		.min_output = 0,
		.max_integral = 0,
		.min_integral = 0,
		.dt = 1
};

MOTOR_Position_Block linear_motor = {
		.pwm = &linear_pwm,
		.encoder = &linear_enc,
		.pid = &linear_pid,
		.target_position = 0
};


void MOTION_YAW_Init()
{
	PWM_Init(yaw_motor.pwm);
	ENC_Init(yaw_motor.encoder);
}

void MOTION_PITCH_Init()
{
	PWM_Init(pitch_motor.pwm);
	ENC_Init(pitch_motor.encoder);
}

void MOTION_LINEAR_Init()
{
	PWM_Init(linear_motor.pwm);
	ENC_Init(linear_motor.encoder);
}

void MOTION_YAW_Callibration()
{

}

void MOTION_LINEAR_Callibration()
{

}

void MOTION_PITCH_Callibration()
{

}

void MOTION_LINEAR_Set_Position(float target_position)
{
	linear_motor.target_position = target_position;
}
void MOTION_YAW_Set_Position(float target_position)
{
	yaw_motor.target_position = target_position;
}

void MOTION_PITCH_Set_Position(float target_position)
{
	pitch_motor.target_position = target_position;
}

int16_t MOTION_LINEAR_Spin()
{
	return MOTOR_Spin_Position(&linear_motor);
}

int16_t MOTION_YAW_Spin()
{
	return MOTOR_Spin_Position(&yaw_motor);
}

int16_t MOTION_PITCH_Spin()
{
	return MOTOR_Spin_Position(&pitch_motor);
}



