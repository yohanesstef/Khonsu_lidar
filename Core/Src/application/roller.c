/*
 * roller.c
 *
 *  Created on: Nov 6, 2022
 *      Author: ahmadjabar
 */


#include <application/roller.h>

PWM_Block motor0_pwm = {
		.T_pwm = TIM12,
		.H_pwm = &htim9,
		.channel = TIM_CHANNEL_1,
		.port_1 = M0_DIR_A_GPIO_Port,
		.port_2 = M0_DIR_B_GPIO_Port,
		.pin_1 = M0_DIR_A_Pin,
		.pin_2 = M0_DIR_B_Pin
};

ENC_Block motor0_enc = {
		.T_pwm = TIM10,
		.H_pwm = &htim11,
		.channel = TIM_CHANNEL_1,
		.port_1 = M0_DIR_A_GPIO_Port,
		.port_2 = M0_DIR_B_GPIO_Port,
		.pin_1 = M0_DIR_A_Pin,
		.pin_2 = M0_DIR_B_Pin
};

pid_ctrl motor0_pid = {
		.Kd = 0,
		.Ki = 0,
		.Kp = 0,
		.previous_err = 0,
		.integral_err = 0,
		.max_output = 0,
		.min_output = 0,
		.max_integral = 0,
		.min_integral = 0,
		.dt = 1
};

MOTOR_Speed_Block motor0 = {
		.pwm = &motor0_pwm,
		.encoder = &motor0_enc,
		.pid = &motor0_pid,
		.target_speed = 0
};

/**
 */

PWM_Block motor1_pwm = {
		.T_pwm = TIM12,
		.H_pwm = &htim9,
		.channel = TIM_CHANNEL_1,
		.port_1 = M0_DIR_A_GPIO_Port,
		.port_2 = M0_DIR_B_GPIO_Port,
		.pin_1 = M0_DIR_A_Pin,
		.pin_2 = M0_DIR_B_Pin
};

ENC_Block motor1_enc = {
		.T_pwm = TIM10,
		.H_pwm = &htim11,
		.channel = TIM_CHANNEL_1,
		.port_1 = M0_DIR_A_GPIO_Port,
		.port_2 = M0_DIR_B_GPIO_Port,
		.pin_1 = M0_DIR_A_Pin,
		.pin_2 = M0_DIR_B_Pin
};

pid_ctrl motor1_pid = {
		.Kd = 0,
		.Ki = 0,
		.Kp = 0,
		.previous_err = 0,
		.integral_err = 0,
		.max_output = 0,
		.min_output = 0,
		.max_integral = 0,
		.min_integral = 0,
		.dt = 1
};

MOTOR_Speed_Block motor1 = {
		.pwm = &motor1_pwm,
		.encoder = &motor1_enc,
		.pid = &motor1_pid,
		.target_speed = 0
};


void ROLLER_Init()
{
	PWM_Init(motor0.pwm);
	PWM_Init(motor1.pwm);
	ENC_Init(motor0.encoder);
	ENC_Init(motor1.encoder);
}

void ROLLER_Set_Speed(float target_speed)
{
	MOTOR_Set_Speed(&motor0, target_speed);
	MOTOR_Set_Speed(&motor1, target_speed);
}

void ROLLER_Spin(float * output)
{
	output[0] = MOTOR_Spin_Speed(&motor0);
	output[1] = MOTOR_Spin_Speed(&motor1);
}


