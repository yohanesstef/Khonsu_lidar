#include <driver/encoder.h>

void ENC_Init(ENC_Block *encoder)
{
	HAL_TIM_Encoder_Start(encoder->H_pwm, encoder->channel);
}

int16_t ENC_Get_Value(ENC_Block* encoder)
{
	return encoder->T_pwm->CNT;
}

void ENC_Set_Value(ENC_Block* encoder, int16_t value)
{
	encoder->T_pwm->CNT = value;
}
