/*
 * motor.c
 *
 *  Created on: Nov 5, 2022
 *      Author: ahmadjabar
 */

#include <driver/motor.h>


void MOTOR_Position_Init(MOTOR_Position_Block * motorpos)
{
	PWM_Init(motorpos->pwm);
	ENC_Init(motorpos->encoder);
}

void MOTOR_Speed_Init(MOTOR_Speed_Block *motorspd)
{
	PWM_Init(motorspd->pwm);
	ENC_Init(motorspd->encoder);
}

void MOTOR_Set_Speed(MOTOR_Speed_Block * speed_block, float target_speed)
{
	speed_block->target_speed = target_speed;
}

void MOTOR_Set_Position(MOTOR_Position_Block * position_block, float target_position)
{
	position_block->target_position = target_position;
}

int16_t MOTOR_Spin_Position(MOTOR_Position_Block * position_block)
{
	float nilai_out;
	int16_t error = position_block->encoder->T_pwm->CNT;
	nilai_out = pid_compute(position_block->pid, position_block->target_position - error);
	PWM_Set_Value(position_block->pwm, nilai_out);

	return error;
}

int16_t MOTOR_Spin_Speed(MOTOR_Speed_Block * speed_block)
{
	float nilai_out;
	int16_t error = speed_block->encoder->T_pwm->CNT;
	speed_block->encoder->T_pwm->CNT = 0;
	nilai_out = pid_compute(speed_block->pid, speed_block->target_speed - error);
	PWM_Set_Value(speed_block->pwm, nilai_out);

	return error;
}





