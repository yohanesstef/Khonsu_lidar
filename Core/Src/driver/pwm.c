/*
 * pwm.c
 *
 *  Created on: Nov 5, 2022
 *      Author: ahmadjabar
 */

#include <driver/pwm.h>

void PWM_Init(PWM_Block *pwm)
{
	HAL_TIM_PWM_Start (pwm->H_pwm, pwm->channel);
}

void PWM_Set_Value(PWM_Block* pwm, int16_t value)
{
  // 0 CW
  // 1 CCW
  HAL_GPIO_WritePin (pwm->port_1, pwm->pin_1,
		     value > 0 ? GPIO_PIN_SET : GPIO_PIN_RESET);
  HAL_GPIO_WritePin (pwm->port_2, pwm->pin_2,
		     value > 0 ? GPIO_PIN_RESET : GPIO_PIN_SET);

  switch (pwm->channel)
  {
    case TIM_CHANNEL_1:
      pwm->T_pwm->CCR1 = abs (value);
      break;
    case TIM_CHANNEL_2:
      pwm->T_pwm->CCR2 = abs (value);
      break;
    default:
      break;
  }
}



