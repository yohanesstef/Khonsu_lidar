/*
 * pid.c
 *
 *  Created on: Nov 5, 2022
 *      Author: ahmadjabar
 */

#include "pid.h"

float pid_compute(pid_ctrl *pid, float input_error)
{
    float error_p, error_i, error_d, output;

    // kp section
    error_p = pid->Kp * input_error;

    // ki section
    error_i = pid->Ki * pid->integral_err;
    if(error_i > pid->max_integral) error_i = pid->max_integral;
    else if(error_i < pid->min_integral) error_i = pid->min_integral;

    pid->integral_err += input_error;

    // kd section
    error_d = pid->Kd * (input_error-(pid->previous_err))/pid->dt;
    pid->previous_err = input_error;

    // sum section
    output = error_d + error_i + error_p;
    if(output > pid->max_output) output = pid->max_output;
    else if(output < pid->min_output) output = pid->min_output;
    return output;
}


